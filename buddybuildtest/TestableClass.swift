//
//  TestableClass.swift
//  buddybuildtest
//
//  Created by Olexa Boyko on 11.02.18.
//  Copyright © 2018 OneStepSolutions. All rights reserved.
//

import Foundation

class TestableClass {
    
    static let shared = TestableClass()
    
    private init() {}
    
    func mockTest() -> Bool {
        return true
    }
}
